# cloud-init: An Encrypted Web Proxy using NGINX and SSH (Ubuntu 22.04)

![DALL·E - hacker cat ssh tunnel encryption nginx pixel art](https://mataroa.blog/images/946f9f25.png)

Prompt: *hacker cat ssh tunnel encryption nginx pixel art*

## Introduction

NGINX is a robust web server that still maintains a minimal design with nominal resource requirements. The proxy functionality offered by the `stream` module can make it very easy to forward requests through a single server, masking their origin.

NGINX and its proxy features can be blended with SSH tunnels to make private servers available on the public web for a select few. Connecting to a server behind a firewall would normally result in a connection timeout. However, if an SSH tunnel is opened between the proxy and a restricted server, a user with access to the proxy can send a request to the restricted server through the proxy. The proxy can likewise be placed behind its own firewall, limiting access to the proxy to only those with SSH access.

Below is a diagram of this approach, where **alice** is accessing **bob**'s server via **carol**'s proxy.

![tunnel-hop](https://mataroa.blog/images/e95ead49.png)

***

Tunneling through a proxy comes with a handful of security benefits.

1) Users never have direct access to the private servers. Only the proxy has access, and the users instead receive access to the proxy. To revoke a user's access to the proxied servers, the only action needed would be to remove their public key from the proxy.

2) Communications between users and the proxy will be completely encrypted via SSH. This is especially useful if the web server is not utilizing HTTPS, but even if it were, the additional layer of encryption means that onlookers can't even see what server is being requested.

3) Communications between the proxy and the restricted servers will likewise be encrypted, so listening to outgoing traffic from the proxy will yield little insight. At most, onlookers will see where the requests are going, but they will not be able to determine what is being sent or received.

4) Both NGINX streams and SSH tunnels enable port-forwarding, meaning that a private web server can be hosted on a port other than 443 or 80, but the user won't have to account for it in the URL of the website, *eg* "example.com:8080" can be reached via "example.com".

5) When using the `stream` module, any type of TCP/UDP request can be forwarded, as opposed to the more common `http` module. This means that databases, mail servers, and more can sit behind a restrictive entrypoint.

### But...

Of course, this is all at the cost of creating a single point-of-failure. If this proxy becomes compromised, it would provide a route to potentially many restricted applications. Thankfully, a user does not need shell access in order to open a tunnel, so be sure that the proxy user's account does not have shell access to the servers that it is tunneling to. Similarly, users connecting to the proxy can have their shell access removed.

The upside to having a single point-of-failure for inbound connections is that keeping a close eye on one server is much easier than many servers, so assuring that a single proxy is secured is not too daunting a task.

![DALL·E - nginx web proxy pixel art](https://mataroa.blog/images/9bd35b7b.png)

Prompt: *nginx web proxy pixel art*

***

## The Server

To deploy the server, use this cloud-config script on any cloud platform where cloud-init and Ubuntu 22.04 are supported. Once the server is deployed, configure any necessary DNS information. The subsequent scripts assume the proxy's domain as "proxy.example.com". The private server being proxied is "example.com". Be sure to swap these domains out when adopting the scripts for your needs, as well as the public/private key placeholders, *eg* `!!! >>> EXAMPLE <<< !!!`

```yaml
#cloud-config

# general housekeeping
package_update: true
package_upgrade: true
package_reboot_if_required: true

# install NGINX
packages:
  - nginx

# create a user for tunneling
users:
  - name: tunnel
    gecos: A user for tunneling
    shell: /usr/sbin/nologin
    ssh_authorized_keys:
      - '!!! >>> YOUR PUBLIC SSH KEY <<< !!!'

runcmd:
  # configure firewall
  - ufw allow 22/tcp
  - ufw enable

  # load host keys to target server
  - ssh-keyscan -H example.com > /root/.ssh/known_hosts

  # enable tunnel service
  - systemctl enable proxy-example-com

  - reboot

write_files:

    # the SSH keys for the tunnel user
  - path: /root/.ssh/id_ed25519
    owner: root:root
    permissions: '0600'
    content: |
      !!! >>> TUNNEL OPENSSH PRIVATE KEY <<< !!!

    # this public key must be added as an authorized key
    # to example.com, under a user named "tunnel"
  - path: /root/.ssh/id_ed25519.pub
    owner: root:root
    permissions: '0644'
    content: |
      !!! >>> TUNNEL OPENSSH PUBLIC KEY <<< !!!

    # this service will open an SSH tunnel with example.com
    # presumably, tunnel's public key has already been shared
    # with example.com, per the instructions above
  - path: /etc/systemd/system/proxy-example-com.service
    owner: root:root
    permissions: '0500'
    content: |
      [Unit]
      Description=SSH tunnel to example.com
      After=network.target

      [Service]
      Type=simple
      Restart=always
      ExecStart=ssh -N -L 1443:localhost:443 tunnel@example.com

      [Install]
      WantedBy=multi-user.target

    # configure NGINX
    # the stream module will forward requests from 
    # a listening port to a target server and port
  - path: /etc/nginx/nginx.conf
    owner: root:root
    permissions: '0644'
    content: |
      include /etc/nginx/modules-enabled/*.conf;

      user www-data;
      worker_processes auto;
      pid /run/nginx.pid;

      stream {
        log_format basic '$remote_addr [$time_local] $protocol $status '
                         '$bytes_sent $bytes_received $session_time '
                         '"$upstream_addr" "$upstream_bytes_sent" '
                         '"$upstream_bytes_received" '
                         '"$upstream_connect_time"';

        server {
          listen      1443;
          proxy_pass  127.0.0.1:1443;
          access_log  /var/log/nginx/access-example-com.log basic;
          error_log   /var/log/nginx/error-example-com.log;
        }
      }


```

***

## The Client

This script will handle opening a tunnel to the proxy. Name it "tunnel" and make it available via the path.

```bash
#!/bin/bash

case $1 in
	start)
		# open tunnel without a shell and in the background
		ssh -Nf proxy-example-com

		# to enable seamless SSL/TLS,
		# add example.com to /etc/hosts
		cat <<- EOF >> /etc/hosts
			# tunnels
			127.0.0.1    example.com
		EOF
	;;

	stop)
		# close the tunnel
		ssh -O exit proxy-example-com

		# remove entries from /etc/hosts
		sed -i '/# tunnels/d' /etc/hosts
		sed -i '/127.0.0.1\s*example.com/d' /etc/hosts
	;;

	*) echo 'valid options ( start | stop )'
esac

```

This script will only work with SSH multiplexing enabled. That can be done by setting the `ControlMaster` and `ControlPath` directives in `~/.ssh/config`.

```txt
Host proxy-*
    Hostname proxy.example.com
    ControlMaster auto
    ControlPath ~/.ssh/socket/%C-%n
    User tunnel

Host proxy-example-com
    LocalForward 443 localhost:1443
```

*Note: a directory named "socket" will need to be created in `~/.ssh`*

The `%C` placeholder will result in a hash of the local hostname, remote hostname, port, and remote user, or `%l`, `%h`, `%p`, and `%r`, respectively. The `%n` placeholder will pass the remote hostname as specified on the command line. In this case, it would be `proxy-example-com`.

Appending the original hostname to the connection hash (`%C`) will allow separate tunnels to be opened through the same proxy, each with unique sessions. Ironically, this is the exact opposite of multiplexing, which aims to allow multiple sessions to share a single connection. The main purpose for using multiplexing here is to enable easily cleaning up an SSH connection that has been sent to the background with the `-f` option. Closing a background multiplex connection can be done by sending an `exit` command to the `proxy-example-com` host with the `-O` option.

```bash
ssh -O exit proxy-example-com
```

Finally, a service can be used to execute the tunnel script on start up.

```txt
[Unit]
Description=SSH tunnel to proxy.example.com
After=network.target

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=tunnel start
ExecStop=tunnel stop

[Install]
WantedBy=multi-user.target
```

The command `systemctl enable` will start the service upon reboot, assuming the file is accessible via `/etc/systemd/system`. If ever the tunnel needs to be recreated, stop the service, and deploy the new tunnel. Once the new tunnel has finished provisioning and the domain name has been pointed to the new IP address, reload the host keys by running `sudo ssh-keyscan -H proxy.example.com >> ~/.ssh/known_hosts`. Afterwards, the tunnel connection can be reopened by starting the service once more.

![DALL·E - hacker](https://mataroa.blog/images/eb71373d.png)

Prompt: *hacker*

***

## A Fair Warning

The examples given here should be seen as a "quick-and-dirty" approach that will achieve a working solution quickly. There are some potential improvements that can make the connection more secure.

1) Don't set private keys in plaintext (see line 43 of tunnel.yml). Instead, retrieving the keys from a keystore or vault using a temporary access key would be a more secure route to take.

2) This approach opens a tunnel from the proxy to the server. If the web server were to instead open a reverse tunnel from itself to the proxy, then compromising the proxy would not likewise leave an opening to the server. This is because the server would be connecting to the proxy, and not vice versa. However, a public key for every service reverse tunneling to the proxy would need to be added to the proxy server's authorized keys.

3) DNS configuration can be automated rather easily. The following script is an example of how a server can set its own domain on Cloudflare.

    ```bash
    #!/bin/bash
    cloudflareurl=https://api.cloudflare.com/client/v4/zones/$CLOUDFLARE_ZONE/dns_records/$CLOUDFLARE_DNS_TUNNEL
    ipaddr=($(hostname -I))
    ipaddr=${ipaddr[0]}
    requestdata="{\"content\":\"$ipaddr\"}"
    curl -X PATCH $cloudflareurl -H "Authorization: Bearer $CLOUDFLARE_API_TOKEN" -H "Content-Type: application/json" --data $requestdata
    ```

4) SSH certificates are generally more secure than SSH keys, as they can have expiration dates. However, certificates aren't without their own challenges (such as renewal after expiration), so they were excluded from this demonstration.

5) Everything else. There are undoubtedly many improvements that could be made to this approach. If you have one you'd like to share, feel free to add a comment below.

## Final Thoughts

There are a lot of ways to proxy and there are even more ways to approach tunneling. When you combine the two techniques, the already-numerous possibilities are compounded. Feel free to toy with these scripts to your liking, maybe by swapping tunneling for reverse tunneling, dumping HTTPS Passthrough for HTTPS Termination, or wrapping tunnels within tunnels. Every problem demands its own solution, and with practice, it's easy to find ways that these techniques can strengthen - or even compromise - an application.

Happy Hacking!

z3c0

![DALL·E - expressionist painting of guy fawkes](https://mataroa.blog/images/4e7bd3f5.png)

Prompt: *expressionist painting of guy fawkes*

## Additional Reading

\[1\]: [capabilities(7) - Linux Manual Page](https://man7.org/linux/man-pages/man7/capabilities.7.html)

\[2\]: [DNSimple - How HTTPS Works](https://howhttps.works/the-keys/)

\[3\]: [Teleport - SSH Handshake Explained](https://goteleport.com/blog/ssh-handshake-explained/)

\[4\]: [Circumventing Network Security via SSH Tunneling](https://community.infosecinstitute.com/discussion/67117/circumventing-network-security-via-ssh-tunneling)

\[5\]: [NGINX - ngx_stream_core_module](https://nginx.org/en/docs/stream/ngx_stream_core_module.html)
